import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../../services/weather.service'
import { LocationService } from '../../services/location.service';

@Component({
  selector: 'app-local-weather',
  templateUrl: './local-weather.component.html',
  styleUrls: ['./local-weather.component.scss']
})
export class LocalWeatherComponent implements OnInit {
  city = '';
  temp_f; temp_c; temp; measure = 'F';
  description: '';
  wind_speed: '';
  icon = '01d';

  constructor(private weatherService: WeatherService,
    private locationService: LocationService) { }

  ngOnInit() {
    this.locationService.getLocation({})
      .subscribe((p) => this.getLocation(p));
  }
  getLocation(loc) {
    this.city = loc.city + ', ' + loc.region_name;
    const coords = { latitude: loc.latitude, longitude: loc.longitude };
    this.weatherService.getWeather(coords)
      .subscribe((w) => this.getWeather(w))
  }
  getWeather(weather) {
    this.description = weather.weather[0].description;
    this.wind_speed = weather.wind.speed;
    this.icon = weather.weather[0].icon;
    this.temp_f = +weather.main.temp;
    this.temp = this.temp_f;
    this.temp_c = Math.round((this.temp_f - 32) * (5 / 9));
    if (this.city === ', ') { this.city = weather.name };
  }
  toggleTemp() {
    if (this.measure === 'F') {
      this.temp = this.temp_c;
      this.measure = 'C';
    } else {
      this.temp = this.temp_f;
      this.measure = 'F';
    }
  }
}
