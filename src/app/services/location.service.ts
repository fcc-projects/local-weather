import { Injectable } from '@angular/core';
import { Http, Response, Jsonp, URLSearchParams } from '@angular/http';


@Injectable()
export class LocationService {

  constructor(private jsonp: Jsonp) { }

  getLocation = (position) => {
    const apiUrl = 'http://freegeoip.net/json';
    const params = new URLSearchParams();
    params.set('callback', 'JSONP_CALLBACK');
    return this.jsonp.get(apiUrl, { search: params }).map(response => response.json());
  }
}